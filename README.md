# Linguistique-cnrtl

Ajoute au menu contextuel de Google Chrome une option de recherche du mot sélectionné parmi les différentes ressources linguistiques du site cnrtl.fr.


## Screenshot

![Menu contextuel pour la recherche](https://bitbucket.org/ealprr/linguistique-cnrtl/wiki/recherche-cnrtl.png)


## Installation

Télécharger simplement le fichier crx depuis chrome/chromium : [recherche-cnrtl.crx](https://bitbucket.org/ealprr/linguistique-cnrtl/downloads/recherche-cnrtl.crx)


## Licence

GPL3

