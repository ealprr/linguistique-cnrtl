// Fonction d'ouverture de la recherche sur le site
function searchOnClick(info, tab) {
  if (!info.selectionText || info.selectionText.length == 0) {
   return;
 }
 var maxLength = 1024;
 var searchText = (info.selectionText.length <= maxLength) ?
                  info.selectionText : info.selectionText.substr(0, maxLength);
 for (var j = 0; j < menuItem[0].length; j++) {
    if (info.menuItemId == menuItem[0][j]) {
        var url = "http://www.cnrtl.fr/"+menuItem[2][j]+"/"+escape(searchText);
    }
 }
 chrome.tabs.create({"url": url});
}

// Cr�ation du menu

var prefs = {};
if (!localStorage.prefs) {
  // Default to notifications being on.
  prefs.item_morphologie=true;
  prefs.item_lexicographie=true;
  prefs.item_etymologie=true;
  prefs.item_synonymie=true;
  prefs.item_antonymie=true;
  prefs.item_proxemie=true;
  prefs.item_concordance=true;
  localStorage.prefs = JSON.stringify(prefs);
}
prefs = JSON.parse(localStorage.prefs);
var menuItem = new Array(new Array(),new Array(),new Array());
if (prefs.item_morphologie) { menuItem[0].push(0);menuItem[1].push("morphologie");menuItem[2].push("morphologie");}
if (prefs.item_lexicographie) { menuItem[0].push(0);menuItem[1].push("d�finition");menuItem[2].push("definition");}
if (prefs.item_etymologie) { menuItem[0].push(0);menuItem[1].push("�tymologie");menuItem[2].push("etymologie");}
if (prefs.item_synonymie) { menuItem[0].push(0);menuItem[1].push("synonymie");menuItem[2].push("synonymie");}
if (prefs.item_antonymie) { menuItem[0].push(0);menuItem[1].push("antonymie");menuItem[2].push("antonymie");}
if (prefs.item_proxemie) { menuItem[0].push(0);menuItem[1].push("prox�mie");menuItem[2].push("proxemie");}
if (prefs.item_concordance) { menuItem[0].push(0);menuItem[1].push("concordance");menuItem[2].push("concordance");}

var parent = chrome.contextMenus.create({"title": "Rechercher '%s' sur cnrtl.fr", "contexts":["selection"]});
for (var i = 0; i < menuItem[0].length; i++) {
  menuItem[0][i] = chrome.contextMenus.create({"title": menuItem[1][i], "contexts":["selection"], "parentId": parent, "onclick": searchOnClick});
}